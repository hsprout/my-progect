const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: path.join(__dirname, './src/index'),
  output: {
    filename: '[hash].bundle.js',
    path: path.resolve(__dirname, './dist'),
  },
  devtool: 'source-map',
  devServer: {
    contentBase: path.resolve(__dirname, "dist"),
    compress: true,
    port: 8080,
    host: '127.0.0.1',
    open: true
  },
  module: {
    rules:[
      {
        test: /\.js$/,    // 标识
        loader: 'babel-loader',   // 转换
        exclude: /node_modules/
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'ts-loader'
      },
      {
        test: /\.[(png)|(obj)|(json)]$/,
        loader: "file-loader"
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
    ]
  },
  resolve: {
    alias: {
      '@/': path.resolve(__dirname, 'src/'),
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: '标题',
      template: './index.html',
    })
  ]
};
